package mysql

import (
	"fmt"
	"gitee.com/hfnzz_admin/toolbox-go/conf"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func InitMysql(conf conf.MysqlConf) (mysqlDB *gorm.DB, err error) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local&timeout=%ds", conf.MysqlUser, conf.MysqlPwd,
		conf.MysqlHost, conf.MysqlPort, conf.MysqlDb, conf.Timeout)
	mysqlDB, err = gorm.Open(mysql.Open(dsn))
	if err != nil {
		return
	}
	db, err := mysqlDB.DB()
	if err != nil {
		return
	}
	err = db.Ping()
	return
}
