package redis

import (
	"gitee.com/hfnzz_admin/toolbox-go/conf"
	"github.com/gomodule/redigo/redis"
	"log"
	"time"
)

// Init 初始化Redis连接池
func Init(conf conf.RedisConf) (redisClient *redis.Pool) {
	redisClient = &redis.Pool{
		MaxIdle:     conf.RedisMaxIdle,
		MaxActive:   conf.RedisMaxActive,
		IdleTimeout: conf.RedisIdleTimeout * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", conf.RedisHost)
			if err != nil {
				log.Println(err.Error())
				return nil, err
			}
			if conf.RedisAuth != "" {
				_, _ = c.Do("AUTH", conf.RedisAuth)
			}
			return c, nil
		},
	}
	return
}
