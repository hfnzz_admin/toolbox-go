module gitee.com/hfnzz_admin/toolbox-go

go 1.16

require (
	github.com/aws/aws-sdk-go v1.40.2
	github.com/beego/beego/v2 v2.0.1
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/teris-io/shortid v0.0.0-20201117134242-e59966efd125
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.12
)
