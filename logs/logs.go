package logs

import (
	"fmt"
	"github.com/beego/beego/v2/core/logs"
	"os"
	"path/filepath"
	"strings"
)

var log *logs.BeeLogger
var debugMode bool

func init() {
	if debugMode {
		return
	}
	log = logs.NewLogger(10)
	err := log.SetLogger(logs.AdapterFile, `{"filename":"`+getCurrentDirectory()+`/logs/log.log","rotateperm":"777","perm":"777","maxdays":3}`)
	if err != nil {
		fmt.Println("日志初始化失败：", err.Error())
	}
	log.EnableFuncCallDepth(true)
	log.SetLogFuncCallDepth(3)
	log.Async()
}

func SetDebugMode() {
	debugMode = true
}

func Info(format string, v ...interface{}) {
	if debugMode {
		fmt.Println(fmt.Sprintf("[INFO] %s", fmt.Sprintf(format, v...)))
		return
	}
	log.Info(format, v...)
}

func Err(format string, v ...interface{}) {
	if debugMode {
		fmt.Println(fmt.Sprintf("[ERR] %s", fmt.Sprintf(format, v...)))
		return
	}
	log.Error(format, v...)
}

func getCurrentDirectory() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0])) //返回绝对路径  filepath.Dir(os.Args[0])去除最后一个元素的路径
	if err != nil {
		Err(err.Error())
	}
	return strings.Replace(dir, "\\", "/", -1) //将\替换成/
}
