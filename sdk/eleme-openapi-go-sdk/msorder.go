// 商家服务中台服务
package elemeOpenApi

// 服务单-列表查询
// queryListRequest 服务单列表查询入参
func (msorder *Msorder) QueryList(queryListRequest_ interface{}) (interface{}, error) {
	params := make(map[string]interface{})
	params["queryListRequest"] = queryListRequest_
	return APIInterface(msorder.config, "eleme.msorder.queryList", params)
}

// 投诉单-明细查询
// queryComplaintDetailRequest 投诉单详情入参
func (msorder *Msorder) QueryComplaintDetail(queryComplaintDetailRequest_ interface{}) (interface{}, error) {
	params := make(map[string]interface{})
	params["queryComplaintDetailRequest"] = queryComplaintDetailRequest_
	return APIInterface(msorder.config, "eleme.msorder.queryComplaintDetail", params)
}

// 投诉单-协商同意
// complaintAgreeRequest 商户同意投诉入参
func (msorder *Msorder) ComplaintAgree(complaintAgreeRequest_ interface{}) (interface{}, error) {
	params := make(map[string]interface{})
	params["complaintAgreeRequest"] = complaintAgreeRequest_
	return APIInterface(msorder.config, "eleme.msorder.complaintAgree", params)
}

// 投诉单-申请平台介入
// complaintApplyPlatformRequest 商家申请平台介入入参
func (msorder *Msorder) ComplaintApplyPlatform(complaintApplyPlatformRequest_ interface{}) (interface{}, error) {
	params := make(map[string]interface{})
	params["complaintApplyPlatformRequest"] = complaintApplyPlatformRequest_
	return APIInterface(msorder.config, "eleme.msorder.complaintApplyPlatform", params)
}

// 投诉单-发券
// complaintSendVoucherRequest 商家发券入参
func (msorder *Msorder) ComplaintSendVoucher(complaintSendVoucherRequest_ interface{}) (interface{}, error) {
	params := make(map[string]interface{})
	params["complaintSendVoucherRequest"] = complaintSendVoucherRequest_
	return APIInterface(msorder.config, "eleme.msorder.complaintSendVoucher", params)
}
