// 招聘市场服务
package elemeOpenApi

// 简历回传
// resume 简历信息
func (recruitment *Recruitment) UploadResume(resume_ interface{}) (interface{}, error) {
	params := make(map[string]interface{})
	params["resume"] = resume_
	return APIInterface(recruitment.config, "eleme.recruitment.uploadResume", params)
}

// 职位状态变更
// positionId 职位id
// status 变更状态
func (recruitment *Recruitment) UpdateJobPositionStatus(positionId_ string, status_ interface{}) (interface{}, error) {
	params := make(map[string]interface{})
	params["positionId"] = positionId_
	params["status"] = status_
	return APIInterface(recruitment.config, "eleme.recruitment.updateJobPositionStatus", params)
}

// 简历状态变更
// resumeId 简历id
// status 变更状态
func (recruitment *Recruitment) UpdateResumeStatus(resumeId_ string, status_ interface{}) (interface{}, error) {
	params := make(map[string]interface{})
	params["resumeId"] = resumeId_
	params["status"] = status_
	return APIInterface(recruitment.config, "eleme.recruitment.updateResumeStatus", params)
}

// 回传职位曝光数据
// positionId 职位id
// exposeCount 曝光数量
// visitCount 访问数量
// dataDate 数据日期
func (recruitment *Recruitment) UpdatePositionExposeData(positionId_ string, exposeCount_ int, visitCount_ int, dataDate_ string) (interface{}, error) {
	params := make(map[string]interface{})
	params["positionId"] = positionId_
	params["exposeCount"] = exposeCount_
	params["visitCount"] = visitCount_
	params["dataDate"] = dataDate_
	return APIInterface(recruitment.config, "eleme.recruitment.updatePositionExposeData", params)
}

// 获取商户目前在架以及审核中岗位
func (recruitment *Recruitment) GetShopJobInfos() (interface{}, error) {
	params := make(map[string]interface{})
	return APIInterface(recruitment.config, "eleme.recruitment.getShopJobInfos", params)
}
