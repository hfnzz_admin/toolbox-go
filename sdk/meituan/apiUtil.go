package meituan

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"
)

type APIResponse struct {
	Code string      `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

func ApiDo(config *Config, urlStr string, params map[string]string) (interface{}, error) {
	req := geneAPIRequest(config, params)
	resp, err := http.Post(urlStr,
		"application/x-www-form-urlencoded",
		strings.NewReader(req),
	)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	ret, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var res APIResponse
	if err = json.Unmarshal(ret, &res); err != nil {
		return nil, err
	}
	if res.Code != "OP_SUCCESS" {
		return nil, errors.New(res.Msg)
	}
	return res.Data, nil
}

func geneAPIRequest(config *Config, biz map[string]string) string {
	params := make(map[string]string)
	params["appAuthToken"] = config.Token
	params["charset"] = "UTF-8"
	params["timestamp"] = strconv.FormatInt(time.Now().Unix(), 10)
	params["developerId"] = config.DeveloperId
	params["businessId"] = "2"
	params["version"] = "2"
	j, _ := json.Marshal(biz)
	data := string(j)
	params["biz"] = data
	sign := getSign(params, config.SignKey)
	params["sign"] = sign
	var uri url.URL
	query := uri.Query()
	for k, v := range params {
		query.Add(k, v)
	}
	return query.Encode()
}

func getSign(params map[string]string, signKey string) string {
	var keys []string
	for k := range params {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	paramsStr := signKey
	for _, k := range keys {
		paramsStr = paramsStr + k + params[k]
	}
	h := sha1.New()
	h.Write([]byte(paramsStr))
	bs := h.Sum(nil)
	sign := hex.EncodeToString(bs)
	return sign
}
