package meituan

type APIClient struct {
	Config Config
	Order  Order
}

type Order struct {
	Config *Config
}

func NewAPIClient(config Config) APIClient {
	client := APIClient{}
	client.Config = config
	client.Order.Config = &config
	return client
}
