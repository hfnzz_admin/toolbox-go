package meituan

// Confirm 商家确认接单
func (o Order) Confirm(orderId string) error {
	url := "https://api-open-cater.meituan.com/waimai/order/confirm"
	params := make(map[string]string)
	params["orderId"] = orderId
	_, err := ApiDo(o.Config, url, params)
	return err
}

// QueryById 根据订单Id查询订单
func (o Order) QueryById(orderId string) (interface{}, error) {
	url := "https://api-open-cater.meituan.com/waimai/order/queryById"
	params := make(map[string]string)
	params["orderId"] = orderId
	ret, err := ApiDo(o.Config, url, params)
	return ret, err
}
