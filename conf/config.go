package conf

import "time"

type RedisConf struct {
	RedisPrefix      string
	RedisHost        string
	RedisAuth        string
	RedisMaxIdle     int
	RedisMaxActive   int
	RedisIdleTimeout time.Duration
}

type MysqlConf struct {
	MysqlHost string
	MysqlUser string
	MysqlPwd  string
	MysqlDb   string
	MysqlPort int
	Timeout   int
}
