package etime

import "C"
import (
	"log"
	"time"
)

type DateTime time.Time

const (
	DateYYYYMMDDLayout       = "20060102"
	DateYYYYMMDDHHmmssLayout = "20060102150405"
	DateYYYYMMDDHHLayout     = "2006010215"
	DateLayout               = "2006-01-02"
	DateTimeLayout           = "2006-01-02 15:04:05"
	BuildTimeLayout          = "2006.0102.150405"
	CBuildTimeLayout         = "Jan  _2 2006 15:04:05"
)

func (dt DateTime) String() string {
	return time.Time(dt).Format(DateTimeLayout)
}

func (dt *DateTime) ToString() string {
	if dt == nil {
		return ""
	}
	return time.Time(*dt).Format(DateTimeLayout)
}

func TimeNowStr() string {
	return time.Now().Format(DateTimeLayout)
}

func TimeToStr(time time.Time) string {
	if time.IsZero() {
		return ""
	}
	return time.Format(DateTimeLayout)
}

func TimeStrToTime(value string) time.Time {
	tm, err := time.ParseInLocation(DateTimeLayout, value, time.Local)
	if err != nil {
		log.Println(err)
	}
	return tm
}

func (dt *DateTime) ToTime() time.Time {
	if dt == nil {
		return time.Now()
	}
	tm, err := time.ParseInLocation(DateTimeLayout, dt.String(), time.Local)
	if err != nil {
		log.Println(err)
	}
	return tm
}

func TimeYYYYMMDD(t time.Time) string {
	return t.Format(DateYYYYMMDDLayout)
}
func StrYYYYMMDDToTime(value string) time.Time {
	tm, err := time.ParseInLocation(DateYYYYMMDDLayout, value, time.Local)
	if err != nil {
		log.Println(err)
	}
	return tm
}

func TimeYYYYMMDDHHmmss(t time.Time) string {
	return t.Format(DateYYYYMMDDHHmmssLayout)
}
func StrYYYYMMDDHHmmssToTime(value string) time.Time {
	tm, err := time.ParseInLocation(DateYYYYMMDDHHmmssLayout, value, time.Local)
	if err != nil {
		log.Println(err)
	}
	return tm
}
func TimeYYYYMMDDHH(t time.Time) string {
	return t.Format(DateYYYYMMDDHHLayout)
}
func StrYYYYMMDDHHToTime(value string) time.Time {
	tm, err := time.ParseInLocation(DateYYYYMMDDHHLayout, value, time.Local)
	if err != nil {
		log.Println(err)
	}
	return tm
}

func Now() *DateTime {
	n := DateTime(time.Now())
	return &n
}

func ToDateTime(time time.Time) *DateTime {
	n := DateTime(time)
	return &n
}

func StrToDateTime(strtime string) *DateTime {
	if strtime == "" {
		return nil
	}
	n := DateTime(TimeStrToTime(strtime))
	return &n
}

func GetBeforeTime(seconds time.Duration) time.Time {
	s, _ := time.ParseDuration("-1s")
	return time.Now().Add(s * seconds)
}
